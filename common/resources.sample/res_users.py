# -*- coding: utf-8 -*-
from ..odoo_resource import OdooResource
from ..tools import api


class ResUsers(OdooResource):

    odoo_model = 'res.users'
    odoo_fields = [
        'id',
        'create_date',
        'write_date',
        'login',
        'name',
        'active'
    ]


class ResUsersList(ResUsers):
    pass

api.add_resource(ResUsersList, '/res_users')
api.add_resource(ResUsers, '/res_users/<res_id>')
